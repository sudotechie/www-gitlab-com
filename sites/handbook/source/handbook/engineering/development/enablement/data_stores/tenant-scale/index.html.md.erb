---
layout: handbook-page-toc
title: Tenant Scale Group
description: "The Tenant Scale Group is the direct outcome of applying our value of Iteration to the direction of the Database Scalability Working Group."
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## About

The Tenant Scale group (formerly Pods or Sharding group) is part of the [Data
Stores stage](/handbook/engineering/development/enablement/data_stores/). We
offer support for groups, projects, and user profiles within our product, but
our main focus is a long-term horizontal scaling solution for GitLab.

This page covers processes and information specific to the Tenant Scale group.
See also the [direction page](/direction/enablement/tenant-scale/) and the
[features we support per category](/handbook/product/categories/features/#data-storestenant-scale-group).

### Contact

To get in touch with us, it's best to create an issue in the relevant
project (typically [GitLab](https://gitlab.com/gitlab-org/gitlab)) and add the
`~"group::tenant scale"` label, along with any other appropriate labels.

For urgent items, feel free to use the Slack channel (internal): [#g_tenant-scale](https://gitlab.slack.com/archives/g_tenant-scale).

### Vision

There are multiple proposals and ideas to increase horizontal scalability via
solutions such as database sharding and tenant isolation. The objective of this
group is to explore, iterate on, validate, and lead implementation of proposals
to provide a solution to accommodate GitLab.com's daily-active user growth.

As we brainstorm and iterate on horizontal scalability proposals, we will
provide implementation details, prototypes, metrics, demos, and documentation to
support our hypotheses and outcomes.

### Goals

The executive summary goals for the Tenant Scale group include:

- Support GitLab.com's daily-active user growth
- Do not allow a problem with any given data store to affect all users
- Minimize or eliminate complexity for our self-managed use-case

### Team Members

The following people are permanent members of the Tenant Scale group:

<%= direct_team(manager_slug: 'arturo-herrero') %>

### Stable Counterparts

The following members of other functional teams are our stable counterparts:

<%= stable_counterparts(
  role_regexp: /(Tenant Scale|Principal Engineer, Data Stores|Distinguished Engineer, Ops and Enablement)/,
  direct_manager_role: 'Engineering Manager, Tenant Scale'
) %>

### Team IC Gearing

**Exception Ratio**: 1 Distinguished Engineer, multiple Staff Engineers.

**Justification**: The Tenant Scale group was formed to implement a sharding
solution to solve our database limitations as outlined by the [Database
Scalability Working Group](https://about.gitlab.com/company/team/structure/working-groups/database-scalability/).
The project requires deep knowledge of both PostgreSQL and the application
itself. As the working group has noted, _this problem cannot be solved to meet
our needs and requirements if we limit ourselves to the database: we must
consider careful changes in the application to make it a reality_. Given the
complexity of the task, we have carefully crafted a team with a mix of interests
and expertise to achieve success. We also anticipate the team will continue to
require the expertise of multiple staff+ level engineers to accomplish our
horizontal scalability goals.

## Meetings

We are a globally distributed group and we communicate mostly asynchronously,
however, we also have synchronous meetings. It's unlikely everyone can attend
those meetings, so we record them and share written summaries ([agenda](https://docs.google.com/document/d/1W7QsQL_2wMLW9KJU5ZEZdyIqYtRC6_bwOoNXJUWNbiU/edit)).
Currently we have the following recurring meetings scheduled:

 - Weekly Monday - Tenant Scale Group Sync (APAC/EMEA) 8:30AM UTC (2:30AM PDT)
 - Weekly Thursday - Tenant Scale Group Sync (EMEA/AMER) 2:30PM UTC (7:30AM PDT)

## Work

The Product Manager compiles the list of issues following
the [product prioritization process](/handbook/product/product-processes/#prioritization),
with input from the team, Engineering Manager, and other stakeholders.
The iteration cycle lasts from the 18th of one month until the 17th of the next month,
and is identified by the GitLab version set to be released on the 22nd.

Engineers are encouraged to work as closely as needed with their stable
counterparts. Quality engineering is included in our workflow via the
[quad planning process](/handbook/engineering/quality/quality-engineering/quad-planning/).

### Milestone Planning

Before starting a milestone, the group coordinates using [planning issues](https://gitlab.com/gitlab-org/tenant-scale-group/group-tasks/-/issues/?label_name%5B%5D=Planning%20Issue).
We follow this process:
- The Product Manager defines the goals of the milestone.
- The team members comment about the issues they consider relevant for the milestone.
- The Product Manager and Engineering Manager work together to decide the final list of issues.
- The whole team reviews the items lined up before the milestone begins.

### What To Work On

The primary source for things to work on is the [milestone prioritization board](https://gitlab.com/gitlab-org/gitlab/-/boards/4621652),
which lists all issues scheduled for the current cycle in priority order (from most to least important): p1,
p2, and p3. You should first pick up issues that have the highest priority, which are listed at the top of the first board column.
When you assign yourself to an issue, you indicate that you are working on it.

If anything is blocking you from getting started with the top issue immediately,
like unanswered questions or unclear requirements, you can skip it, as long as
you put your findings and questions in the issue.
This helps the next engineer who picks up the issue.

Usually issues are not directly assigned to people, except when
a person has clearly the most knowledge or context to work on an issue.

### Development Workflow

We follow the GitLab [engineering workflow](/handbook/engineering/workflow/)
guidelines. To get a high-level overview of the status of all issues in the
current milestone, check the [development workflow board](https://gitlab.com/gitlab-org/gitlab/-/boards/1339417).

As owners of the issues assigned to them, engineers are expected to keep the
workflow labels on their issues up to date. When an engineer starts working an
issue, they mark it with the `workflow::in dev` label as the starting point
and continue [updating the issue throughout development](/handbook/engineering/workflow/#updating-issues-throughout-development).
Before closing an issue, it's important to add the `workflow::complete` label, because this is one
of the requirements for the completed items to appear in the Improvements and Bugs
overview of each month's release post. The process primarily follows this diagram:

``` mermaid
graph LR

  classDef workflowLabel fill:#428BCA,color:#fff;

  A(workflow::in dev):::workflowLabel
  B(workflow::in review):::workflowLabel
  C(workflow::verification):::workflowLabel
  F(workflow::complete):::workflowLabel

  A -- Push an MR --> B
  B -- Merged --> C
  C --> D{Works on production?}
  D -- YES --> F
  F --> CLOSE
  D -- NO --> E[New MR]
  E --> A
```

### Boards

- [Pods: Build](https://gitlab.com/groups/gitlab-org/-/boards/2594854?scope=all&utf8=%E2%9C%93&label_name[]=group%3A%3Apods) for issues assigned specifically to `~"group::pods"`
  - We are following a model similar to the process described in the [Geo Build Board - A short tour](https://www.youtube.com/watch?v=rZW0ou4u-dw&list=PL05JrBw4t0KoY_6FXXVgj7wPE9ZDS4cOw&index=6)
- [Group::Pods Roadmap](https://gitlab.com/groups/gitlab-org/-/epics?state=opened&page=1&sort=start_date_desc&label_name[]=group::pods)

### Everyone Can Contribute

Currently the Tenant Scale group is in the early stages of working out what the finished product will be. The task ahead of us is vast and complex. The only way we are going to be able to break this down and iterate on it is to build proofs-of-concept.

Currently we are in the proof-of-concept stage, in most cases we will need to take them to their logical conclusion. We might reach out to other GitLab teams to help us validate some of the assumptions we have had to make to in order to continue this exploration process. We are not afraid to determine that a proof-of-concept should not be continued. We will take what we learn from the process and apply it to the next one so we can iterate on PoC's in an efficient way.

We have [short toes](https://about.gitlab.com/handbook/values/#short-toes) and welcome contributions, we are also very happy to talk about Cells with anyone that is interested. If you have a question, comment or just want generally know what we are up to and how we are going about it feel free to contact us, we'll make time.

### Demos

It is expected that we share progress early and often by creating recorded demos. There will be specific requests for demonstration topics, but unscheduled demonstrations are welcome too. When recording a demo it does not need to be polished or perfect. Guidelines below:

- Keep it simple, it doesn't need to be polished
- For planned demos, open an issue to discuss the topics to be demonstrated with the team. Planned demos would include end of milestone or major functionality
- For ad hoc, spur of the moment or other interesting discoveries that you would like to demo, just create it and announce in Slack
- Demonstrate functionality, even if incomplete and only working locally
- The [Tenant Scale group](https://www.youtube.com/playlist?list=PL05JrBw4t0Kr_xKd1sBqZ5ap_MJnniixO) playlist on GitLab Unfiltered contains all the Tenant Scale group demos and other recordings. As soon as the first Cells videos are uploaded a Cells playlist will be created and this page will be updated with the appropriate link.
- Announce the recording availablity to the #g_tenant-scale slack channel

We should be aiming for sharing demos on a weekly basis.

### Proofs of Concept

There will be many opportunities for us to undertake proofs of concept (POC). When we do, we agree to timebox the POC to 4 weeks and ideally to line up with milestone boundaries. When we need to make exceptions to this agreement we will employ [multimodal communication](/handbook/communication/#multimodal-communication) to ensure all stakeholders are properly apprised. 

### Implementation Plans

In the early phases of our work we will create implementation plans for review. As we create these implementation plans we will list them below.

- [Cells blueprint](https://docs.gitlab.com/ee/architecture/blueprints/pods/index.html)
- [Migrate CI Tables To New Database Plan](./migrate-ci-tables-to-new-database-plan.html)

## How We Interview Candidates

We use Ruby on Rails to build our product backed by a PostgreSQL database. You'll be a great fit for the team if you have strong database performance and scaling experience. Tenant Scale group members have the chance to put their mark on the end product and how it will be implemented as well as being at the forefront of building the solution.

Our goal is to ensure all candidates we hire are in the best position to make fantastic contributions to GitLab, using their technical knowledge and creative expertise in solving problems.

We hold the bar high in hiring quality engineers but also understand the interview process can feel like a long process. Below are the stages for interviewing for Tenant Scale group that balances the need for hiring for success and expediating the process.

We aim to set up and book in all interviews in at the beginning of the interview process so candidates can see and prepare for what lays ahead for them. Feedback from candidates on the process is welcomed and a channel for that feedback will be provided at the beginning.

### Interview Stages.
1. Screening and Hiring Manager Interview
  * If the Screening call is held by the Hiring Manager the two parts will be conducted at the same time.
  * If the Screening call is held by one of our Technical Recruiters the Hiring Manager call will be held separately.
  * We will talk about a candidate's expectations of the role and life at GitLab as well as talking about their technical skills at a high level and behavoural questions.
1. Technical Assessment
  * Candidates are given access to a Merge Request for them to review before the interview. At the interview the candidate will discuss their review and pair with two technical assessment staff on the call and make improvements.
  * There may be further technical discussions to gauge the candidates technical fit in the role and the team.
  * Provides a chance for the candidate to delve into the techincal aspects of the role and GitLab's tech stack.
1. Peer Team Member
  * A behavioural style interview with a Product Manager or peer team member.
1. Skip-level Manager
  * As per the Peer Team Member interview and a further chance for the candidate to demonstrate their fit for the role.
## Dashboards

<%= partial "handbook/engineering/metrics/partials/_cross_functional_dashboard.erb", locals: { filter_value: "Tenant Scale" } %>
