---
layout: markdown_page
title: "Glossary of delivery terminology"
---

# Goals

This glossary is meant to serve as a guide to help team members and users discuss topics related to the Ops sub-department and more specifically application delivery. It aims to achieve the following:  

* Align concept definitions to improve the effectiveness of communication between Ops team members.
* Reduce the potential for miscommunication.
* Help new team members and community contributors get up to speed faster, reducing the time to productivity.

# Scope

The terms and their definitions outlined in this document are provided in context specifically for the GitLab product. Therefore, these terms may have different meanings to users outside of GitLab.

## Entity definitions

### Environment

An environment is a logical concept that describes part of its target infrastructure with a rich set of metadata. It

* describes the history of deployments,
* contains any deployed configuration,
* can deploy to its target infrastructure,

Environments can be of different types:

* in terms of functionality
   * production
   * non-production
* in terms of lifespan
   * long-living
   * ephemeral

Every environment has a single target infrastructure, a target infrastructure might have many environments. Thus multi-region setups require multiple environments.

You can have multiple instances of a given type (for example by region, provider, applications served). It’s useful to query the system by environment type, to be able to build dashboards around environment types.

Environments might be dynamically created during the pipeline execution, they might be templated.

#### Example

GitLab environments. Examples: gprd, gstg, pre

   * These are provisioned in Terraform
   * The deployed version is stored in the repository together with the configuration of that deployment ([example](https://gitlab.com/gitlab-com/gl-infra/k8s-workloads/gitlab-com/-/merge_requests/1099 ))
     * The version of the app deployed is part of the environment configuration
   * A log of deployments: a deployment is a CI job that matches up to an environment

### Target infrastructure

A target infrastructure in an immutable entity that contains one or more environment, along with applications that run in that environment. A target infrastructure typically runs multiple applications, but is not application specific. 

From GitLab's perspective, target infrastructures are considered external systems. The target infrastructure is responsible for the authenticating of GitLab with the infrastructure. Authorization is managed based on the respective best practices of the given infrastructure (e.g. IAM, RBAC). Accesses may be restricted to certain jobs or users and may be logged for compliance and be programmable. For example, retrieving and returning a JWT token. 
